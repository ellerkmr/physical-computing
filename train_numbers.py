# Import the modules
from sklearn.externals import joblib
from sklearn import datasets
from skimage.feature import hog
from sklearn.svm import *
from sklearn.kernel_approximation import *
from sklearn.linear_model import *
import numpy as np

# get the training data set
dataset = datasets.fetch_mldata("MNIST Original")

# create arrays of features and labels
features = np.array(dataset.data, 'int16')
labels = np.array(dataset.target, 'int')

#rbf_feature = RBFSampler(gamma=1, random_state=1)
#features = rbf_feature.fit_transform(features)


# calculate the hog
list_hog_fd = []
for feature in features:
    fd = hog(feature.reshape((28, 28)), orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise=False, block_norm='L1')  #
    list_hog_fd.append(fd)
hog_features = np.array(list_hog_fd, 'float64')

# create a vector support machine
clf = LinearSVC()
#clf = NuSVC()
#gamma = [0.01, 0.05, 0.1]
gamma = 0.05
#c = [0.5, 1, 5]
c = 5
#clf = SVC(kernel='rbf', C=c, gamma=gamma)
#clf = SGDClassifier()
# do training
clf.fit(hog_features, labels)

# dump a file with the fitted training set
joblib.dump(clf, "digits_cls.pkl", compress=3)
