import cv2
import imutils
import os
import pyttsx3
import sys
import time

# from espeak import espeak
from keras.models import load_model
import numpy as np

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

DEBUG = False

# espeak.set_voice("de")
# espeak.synth("Blaukraut bleibt Blaukraut und Brautkleid bleibt Brautkleid")

engine = pyttsx3.init()
voices = engine.getProperty('voices')
for voice in voices:
    if voice.languages[0] == 'de':
        engine.setProperty('voice', voice.id)
        break

# parse arguments
#ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--image", required=True, help="Path to image to recognize")
#ap.add_argument("-m", "--model", required=True, help="Path to saved classifier")
#args = vars(ap.parse_args())

# read,resize and convert to grayscale
#image = cv2.imread(args["image"])

# decide which algorithm
sort = sys.argv[1]
print("Sort: " + sort)
if (sort != "selection" and sort != "bubble"):
    print("Your sort algorithm is not supported. Use 'selection' or 'bubble'.")
    sys.exit(0)

pic = sys.argv[2]
print(pic)
if pic == "":
    print("Please choose a picture")
    sys.exit(0)
else:
    image = cv2.imread(pic)
#image = cv2.imread("abst_sort.jpg")
image = imutils.resize(image, width=320)
if DEBUG:
    cv2.imshow("Resize", image)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Rectangular kernel with size 5x5
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))

# apply blackhat and otsu thresholding
blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, kernel)
_, thresh = cv2.threshold(blackhat, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
thresh = cv2.dilate(thresh, None)  # dilate thresholded image for better segmentation

# find external contours
(_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
avgCntArea = np.mean([cv2.contourArea(k) for k in cnts])  # contourArea for digit approximation

digits = []
boxes = []

for i, c in enumerate(cnts):
    if cv2.contourArea(c) < avgCntArea / 10:
        continue
    mask = np.zeros(gray.shape, dtype="uint8")  # empty mask for each iteration

    (x, y, w, h) = cv2.boundingRect(c)
    hull = cv2.convexHull(c)
    cv2.drawContours(mask, [hull], -1, 255, -1)  # draw hull on mask
    mask = cv2.bitwise_and(thresh, thresh, mask=mask)  # segment digit from thresh

    digit = mask[y - 8:y + h + 8, x - 8:x + w + 8]  # just for better approximation
    #print("digit: ", digit)
    digit = cv2.resize(digit, (28, 28))
    boxes.append((x, y, w, h))
    digits.append(digit)

digits = np.array(digits)
#model = load_model(args["model"])
model = load_model("simple_mnist_cnn.model")
# digits = digits.reshape(-1,784)    #for Multi-Layer-Perceptron
digits = digits.reshape(digits.shape[0], 28, 28, 1)  # for Convolution Neural Networks
labels = model.predict_classes(digits)

if DEBUG:
    cv2.imshow("Original", image)
    cv2.imshow("Thresh", thresh)

# draw bounding boxes and print digits on them
recognizedNumbers = []  # get the recognized numbers in an array and use it in a sort algorithm
for (x, y, w, h), label in sorted(zip(boxes, labels)):
    recognizedNumbers.append(label)
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 1)
    cv2.putText(image, str(label), (x + 2, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 0), 2)
    if DEBUG:
        cv2.imshow("Recognized", image)
        cv2.waitKey(0)

print("recognized numbers: ", str(recognizedNumbers))
cv2.destroyAllWindows()

if sort == "selection":
    sortStep = 1
    for fillslot in range(len(recognizedNumbers)-1, 0, -1):
        positionOfMax = 0
        for location in range(1, fillslot + 1):
            if recognizedNumbers[location] > recognizedNumbers[positionOfMax]:
                positionOfMax = location

            temp = recognizedNumbers[fillslot]
            recognizedNumbers[fillslot] = recognizedNumbers[positionOfMax]
            recognizedNumbers[positionOfMax] = temp
            # os.system("espeak 'Vertausche {0} und {1}'".format(temp, recognizedNumbers[positionOfMax]))
            # espeak.synth("Blaukraut bleibt Blaukraut und Brautkleid bleibt Brautkleid")
            # while espeak.is_playing:
            #    time.sleep(0.1)
            engine.say('Vertausche {0} und {1}'.format(temp, recognizedNumbers[positionOfMax]))
            engine.runAndWait()
            print("Numbers output %s after sort step %d: " % (str(recognizedNumbers), sortStep))
            sortStep += 1
            input("Press any key")

elif sort == "bubble":
    sortStep = 1
    for passnum in range(len(recognizedNumbers)-1, 0, -1):
        for i in range(passnum):
            if recognizedNumbers[i] > recognizedNumbers[i + 1]:
                temp = recognizedNumbers[i]
                recognizedNumbers[i] = recognizedNumbers[i + 1]
                recognizedNumbers[i + 1] = temp
                # os.system("espeak 'Vertausche {0} und {1}'".format(temp, recognizedNumbers[i + 1]))
                # espeak.synth("Blaukraut bleibt Blaukraut und Brautkleid bleibt Brautkleid")
                # while espeak.is_playing:
                #    time.sleep(0.1)
                # print(recognizedNumbers[i], recognizedNumbers[i + 1])
                engine.say('Vertausche {0} und {1}'.format(recognizedNumbers[i], recognizedNumbers[i + 1]))
                engine.runAndWait()
        print("Numbers output %s after sort step %d: " % (str(recognizedNumbers), sortStep))
        sortStep += 1
        input("Press any key")
